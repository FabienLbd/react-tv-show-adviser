import s from "./style.module.css";
import { TVShowListItem } from "../TVShowListItem/TVShowListItem";

export function TVShowList({ list, onClickItem }) {

console.log(onClickItem);

  return (
    <>
      <div className={s.title}>You may also like:</div>
      <div className={s.list}>
        {list.map((tvShow) => {
          return (
            <span key={tvShow.id} className={s.tv_show_list_item}>
              <TVShowListItem onClick={() => onClickItem(tvShow)} tvShow={tvShow} />
            </span>
          );
        })}
      </div>
    </>
  );
}
