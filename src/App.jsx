import "./global.css";
import s from "./style.module.css";
import { TVShowApi } from "./api/tv-show";
import { useEffect, useState } from "react";
import { BACKDROP_BASE_URL } from "./config";
import { TVShowDetail } from "./components/TVShowDetail/TVShowDetail";
import { Logo } from "./components/Logo/Logo";
import logo from "./assets/images/logo.png";
import { TVShowList } from "./components/TVShowList/TVShowList";
import { SearchBar } from "./components/SearchBar/SearchBar";

export function App() {
  const [currentTVShow, setCurrentTVShow] = useState();
  const [recommendations, setRecommendations] = useState([]);

  async function fetchPopulars() {
    try {
      const populars = await TVShowApi.fetchPopulars();

      if (populars.length > 0) {
        setCurrentTVShow(populars[3]);
      }
    } catch (error) {
      alert("Oups ! Something went wrong ...");
    }
  }

  async function fetchRecommendations(tvShowId) {
    try {
      const recommendations = await TVShowApi.fetchRecommendations(tvShowId);

      if (recommendations.length > 0) {
        setRecommendations(recommendations.slice(0, 10));
      }
    } catch (error) {
      alert("Oups ! Something went wrong ...");
    }
  }

  async function searchTVShow(tvShowName) {
    try {
      const searchResponse = await TVShowApi.fetchByTitle(tvShowName);
      if (searchResponse.length > 0) {
        setCurrentTVShow(searchResponse[0]);
      }
    } catch (error) {
      alert("Oups ! Something went wrong ...");
    }
  }

  useEffect(() => {
    fetchPopulars();
  }, []);

  useEffect(() => {
    if (currentTVShow) {
      fetchRecommendations(currentTVShow.id);
    }
  }, [currentTVShow]);

  return (
    <div
      className={s.main_container}
      style={{
        background: currentTVShow
          ? `linear-gradient(rgba(0,0,0,0.55), rgba(0,0,0,0.55)), url("${BACKDROP_BASE_URL}${currentTVShow.backdrop_path}") no-repeat center / cover`
          : "black",
      }}
    >
      <div className={s.header}>
        <div className="row">
          <div className="col-4">
            <Logo
              image={logo}
              title="Watowatch"
              subtitle="Find a show you may like"
            />
          </div>
          <div className="col-sm-12 col-md-4">
            <SearchBar onSubmit={searchTVShow} />
          </div>
        </div>
      </div>
      <div className={s.tv_show_detail}>
        {currentTVShow && <TVShowDetail tvShow={currentTVShow} />}
      </div>
      <div className={s.recommendations}>
        {recommendations && recommendations.length > 0 && (
          <TVShowList onClickItem={setCurrentTVShow} list={recommendations} />
        )}
      </div>
    </div>
  );
}
